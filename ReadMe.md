# Proxy server for local development

###Before run:

Make a copy of environments

```bash
cp .env.example .env
```

You have to create a structure of project like below

```bash
.
├── proxy-dev-server
│ ├── Dockerfile
│ ├── docker-compose.yml
│ ├── nginx.conf
│ └── makefile
├── react-a
│ ├── package.json
│ ├── package-lock.json
│ └── (others files react-a)
└── react-b
├── package.json
├── package-lock.json
└── (others files react-b)
```

In .env you have to enter names of React apps folders like this

```bash
MAIN_REACT_APP_PATH="react-a"
NEW_REACT_APP_PATH="react-b"
```

In nginx.conf you can change paths for those React apps

```nginx
server {
    listen 80;

    location /platform {
        proxy_pass http://react-b:3002;
    }

    location / {
        proxy_pass http://react-a:3001;
    }
}
```

Build containers

```bash
make build
```

Run projects

```bash
make up
```

After all you can see your project there _[link to open project](http://localhost:8080)_

To stop project

```bash
make down
```
